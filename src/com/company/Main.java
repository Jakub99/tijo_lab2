package com.company;

class Adress {
    private String postalCode;
    private String streetName;
    private String city;
    private String buildingNumber;

    public Adress(String postalCode,
                  String streetName,
                  String city,
                  String buildingNumber) {
        this.postalCode = postalCode;
        this.streetName = streetName;
        this.city = city;
        this.buildingNumber = buildingNumber;
    }
}

class Person {
    private String firstName;
    private String lastName;
    private int age;
    private String email;

    public Person(String firstName,
                  String lastName,
                  int age,
                  String email,
                  String postalCode,
                  String streetName,
                  String city,
                  String buildingNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.email = email;
    }
}

class EmailValidator {
    static public boolean validateEmail(String email) {
        return email.contains("@") && email.contains(".");
    }
}


public class Main {
    public static void main(String[] args) {
	// write your code here
    }
}
